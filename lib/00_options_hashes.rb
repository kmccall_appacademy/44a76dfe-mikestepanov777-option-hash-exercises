def transmogrify(string, options = {})
  defaults = {
    times: 1,
    upcase: false,
    reverse: false
  }
  options = defaults.merge(options)
  if options[:upcase]
    string = string.upcase
  end
  if options[:reverse]
    string = string.reverse
  end
  string * options[:times]
end
